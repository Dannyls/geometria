import math


class Punto():
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "({},{})".format(self.x, self.y)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("{} se encuentra en el primer cuadrante".format(self))
        elif self.x < 0 and self.y > 0:
            print("{} se encuentra en el segundo cuadrante".format(self))
        elif self.x < 0 and self.y < 0:
            print("{} se encuentra en el tercer cuadrante".format(self))
        elif self.x > 0 and self.y < 0:
            print("{} se encuentra en el cuarto cuadrante".format(self))
        elif self.x != 0 and self.y == 0:
            print("{} situado sobre el eje X".format(self))
        elif self.x == 0 and self.y != 0:
            print("{} situado sobre el eje Y".format(self))
        else:
            print("{} localizado sobre el origen".format(self))

    def vector(self, p):
        print("El vector obtenido de {} y {} es ({}, {})".format(self, p, p.x - self.x, p.y - self.y))

    def distancia(self, p):
        dist = math.sqrt((p.x - self.x) ** 2 + (p.y - self.y) ** 2)
        print("La distancia entre los ejes {} y {} es {}".format(self, p, dist))


class Rectangulo:
    punto_inicial: Punto
    punto_final: Punto

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

        self.Base = abs(self.punto_final.x - self.punto_inicial.x)
        self.Altura = abs(self.punto_final.y - self.punto_inicial.y)
        self.Area = self.Base * self.Altura

    def base(self):
            print("La base del rectángulo es {}".format(self.Base))

    def altura(self):
            print("La altura del rectángulo es {}".format(self.Altura))

    def area(self):
            print("El área del rectángulo es %.1f" %self.Area)


A = Punto(2, 3)
print("punto A", A)
B = Punto(5, 5)
print("punto B", B)
C = Punto(-3, -1)
print("punto C", C)
D = Punto(0, 0)
print("punto D", D)

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.vector(B)
B.vector(A)

A.distancia(B)
B.distancia(A)


Resul = Rectangulo(A, B)
Resul.base()
Resul.altura()
Resul.area()